package org.jfge.games.mk2.arena;

import java.io.IOException;
import java.util.Random;

import org.jfge.api.arena.Arena;
import org.jfge.api.arena.ArenaFactory;
import org.jfge.spi.render.ArenaRenderer;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

@Singleton
public final class GorosLairArena implements Provider<Arena> {

	private ArenaRenderer arenaRenderer;

	private ArenaFactory arenaFactory;

	private Arena arena;

	private final String image = "goroslair.png";
	
	@Inject
	public GorosLairArena(@Named("arenaRenderer.mortalKombat2") ArenaRenderer arenaRenderer, ArenaFactory arenaFactory) {
		this.arenaRenderer = arenaRenderer;
		this.arenaFactory = arenaFactory;
	}

	@Override
	public Arena get() {
		if(this.arena == null) {
			try {		
				this.arena = arenaFactory.createArena("/org/jfge/games/mk2/arena/images/" + image, arenaRenderer);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return this.arena;
	}
}
