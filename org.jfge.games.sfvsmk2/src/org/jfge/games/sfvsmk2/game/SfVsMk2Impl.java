package org.jfge.games.sfvsmk2.game;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import org.jfge.api.ai.AiController;
import org.jfge.api.ai.AiControllerParser;
import org.jfge.api.arena.Arena;
import org.jfge.api.arena.ArenaFactory;
import org.jfge.api.collision.CollisionDetector;
import org.jfge.api.engine.Engine;
import org.jfge.api.fighter.Fighter;
import org.jfge.api.game.Game;
import org.jfge.api.game.GameState;
import org.jfge.spi.controller.Controller;
import org.jfge.spi.graphics.Graphics;
import org.jfge.spi.graphics.GraphicsFactory;
import org.jfge.spi.render.ArenaRenderer;
import org.jfge.spi.scene.Scene;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

/**
 * The Class GameImpl.
 */
@Singleton
public final class SfVsMk2Impl implements Game {

	/** The controller. */
	private Controller controller;
	
	/** The engine. */
	private Engine engine;
	
	/** The fighter1. */
	private Fighter fighter1;
	
	/** The fighter2. */
	private Fighter fighter2;
	
	/** The collision detector. */
	private CollisionDetector collisionDetector;
	
	/** The ai controller parser. */
	private AiControllerParser aiControllerParser;
	
	/** The ai controller (one for each fighter). */
	private AiController aiController1;
	private AiController aiController2;
	
	/** The arena. */
	private Arena arena;
	
	/** The name */
	private String name;
	
	/** The states */
	private GameState curState;
	
	private HashMap<String, GameState> states;
	
	private String startState;
	
	/**
	 * Instantiates a new game impl.
	 *
	 * @param logger the logger
	 * @param imageLoader the image loader
	 * @param engine the engine
	 * @param fighters the fighters
	 * @param availableControllers the available controllers
	 * @param collisionDetector the collision detector
	 * @param arenaRenderer the arena renderer
	 * @param loadingScene the loading scene
	 * @param aiControllerParser the ai controller parser
	 * @throws Exception the exception
	 */
	@Inject
	public SfVsMk2Impl(Engine engine, 
			Map<String, Provider<Fighter>> fighters,
			Map<String, Controller> availableControllers,
			CollisionDetector collisionDetector,
			Map<String,Provider<Scene>> scenes,
			Map<String,Provider<Arena>> arenas,
			AiControllerParser aiControllerParser,
			String name, 
			List<GameState> states, 
			String startState) throws Exception {
			
			this.engine = engine;
			this.name = name;
			
			this.states = new HashMap<String, GameState>();
			for(GameState state: states) {
				state.setParent(this);
				this.states.put(state.getName(), state);
			}
			
			this.startState = startState;
		
			this.controller = availableControllers.entrySet().iterator().next().getValue();
			this.collisionDetector = collisionDetector;
			this.aiControllerParser = aiControllerParser;
			this.aiController1 = aiControllerParser.parseFromXmlFile("/org/jfge/games/sfvsmk2/ai/sfvsmk2Ai1.xml");
			this.aiController2 = aiControllerParser.parseFromXmlFile("/org/jfge/games/sfvsmk2/ai/sfvsmk2Ai1.xml");
			
			Scene loadingScene = scenes.get("loadingScreen").get();
			
			engine.addRenderable(loadingScene);
			engine.addUpdatable(loadingScene);
			engine.start();
			
			fighter1 = fighters.get("kano").get();
			fighter2 = fighters.get("liuKang").get();
			
			this.arena = arenas.get("thearmory").get();
			
			engine.removeRenderable(loadingScene);
			engine.removeUpdatable(loadingScene);
			
			engine.addRenderable(this);
			engine.addUpdatable(this);
	}
	
	/* (non-Javadoc)
	 * @see org.jfge.game.Game#end()
	 */
	@Override
	public void end() {
		this.engine.stop();
	}

	/* (non-Javadoc)
	 * @see org.jfge.game.Game#start()
	 */
	@Override
	public void start() {
		this.engine.start();
		this.startState();
	}

	/* (non-Javadoc)
	 * @see org.jfge.engine.Updatable#update()
	 */
	@Override
	public void update() {
		if(curState == null)
			return;
		
		aiController1.handle(fighter1, fighter2);
		aiController2.handle(fighter2, fighter1);
	}

	/* (non-Javadoc)
	 * @see org.jfge.engine.Renderable#render(java.awt.Graphics)
	 */
	@Override
	public void render(Graphics graphics) {
		if(graphics == null)
			return;
		
		if(curState == null)
			return;
		
		this.curState.render(graphics);
		
		/*
    	 * draw arena background should be done in renderer
    	 */
		if(arena.getState() != null){
			arena.render(graphics);
		}
		
    	/*
    	 * render fighters;
    	 */
    	fighter1.render(graphics);
    	fighter2.render(graphics);
    }

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public GameState getState() {
		return curState;
	}

	@Override
	public boolean handle(String event) {
		if(curState == null)
			return false;
		
		return curState.handle(event);
	}

	@Override
	public boolean hasFinalStateReached() {
		if(curState == null)
			return false;
		
		return curState.hasFinalStateReached();
	}

	@Override
	public boolean nextState() {
		if(curState == null)
			return false;
		
		return curState.nextState();
	}

	@Override
	public boolean setState(String state) {
		if (state == null)
			return false;

		/*
		 * looking for needed state in our hashmap
		 */
		GameState gameState = states.get(state);

		if (gameState != null) {
			// set found state to initial state
			gameState.startState();
			
			// setting found state to current state
			this.curState = gameState;
			
		}

		return gameState != null;
	}

	@Override
	public boolean startState() {
		return this.setState(this.startState);
	}

	@Override
	public Engine getEngine() {
		return this.engine;
	}
}
